((magit-log:magit-log-mode "-n256" "--graph" "--color" "--decorate")
 (magit-log:magit-status-mode "-n256" "--decorate")
 (magit-tag "--annotate"))
