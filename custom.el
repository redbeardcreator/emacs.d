(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(custom-safe-themes
   '("1c50040ec3b3480b1fec3a0e912cac1eb011c27dd16d087d61e72054685de345" default))
 '(display-time-mode t)
 '(flycheck-disabled-checkers '(json-jsonlint json-python-json))
 '(helm-buffers-fuzzy-matching t)
 '(package-selected-packages
   '(
     all-the-icons-ibuffer
     apache-mode
     bbcode-mode
     company-math
     company-php
     dim
     dockerfile-mode
     fic-mode
     fira-code-mode
     fireplace
     flycheck
     flycheck-phpstan
     flycheck-yamllint
     flymake-phpstan
     git-gutter
     git-gutter-fringe
     git-timemachine
     gitconfig-mode
     gitignore-mode
     golden-ratio
     helm-git-files
     helm-git-grep
     hipster-theme
     js2-mode
     json-mode
     json-navigator
     json-par
     magit
     magit-delta
     magit-filenotify
     magit-todos
     markdown-toc
     multiple-cursors
     noccur
     rainbow-delimiters
     restclient
     scss-mode
     seq
     sqlup-mode
     string-inflection
     symbol-overlay
     use-package
     wc-goal-mode
     web-mode
     yaml-mode
     yasnippet
     yasnippet-snippets
     ))
 '(recentf-mode t)
 '(shift-select-mode nil)
 '(show-paren-mode t)
 '(sort-fold-case t)
 '(tool-bar-mode nil)
 '(warning-suppress-types
   '((comp)
     (comp)
     ((undo discard-info))
     ((undo discard-info)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Fira Code" :foundry "CTDB" :slant normal :weight normal :height 98 :width normal))))
 '(blamer-face ((t :foreground "#7a88cf" :background nil :height 140 :italic t)))
 '(fixed-pitch ((t (:foreground "olive drab" :family "Fantasque Sans Mono"))))
 '(flyspell-incorrect ((t (:underline (:color "pink" :style wave)))))
 '(markdown-inline-code-face ((t (:inherit font-lock-constant-face)))))
